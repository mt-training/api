## 9. Payment

In this exercise, you will make a payment using the manual gateway. 

### Enable manual gateway

* Open the `Update Manual gateway` request under `gateways` folder.
* Ensure `enabled` is set to `true`.
* Click `Send`. 
* Verify the response.

### Authorize payment

* Open the `Authorize` request under `payments -> manual` folder.
* Ensure gateway is set to `manual`.
* Click `Send`. The new transaction id is saved to a `transactionID` variable.
* Verify the response.

### Capture payment

* Open the `Capture a transaction` request under `transactions` folder.
* Click `Send`.
* Verify the response.

[Next: 10. Orders](./order.md)
