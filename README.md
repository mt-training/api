## Using the Elastic Path Commerce Cloud API

In these exercises, you will learn how to use the API to make a sample set of requests, covering authentication and products to customers and checkout. 

### Prerequisites

You need to have the following tools installed:

* Postman (free version)
* Elastic Path Commerce Cloud account that you can use for this training

### Objectives

Make API requests and validate the responses for scenarios in the following areas:

* Authentication
* Products
* Settings
* Promotions
* Carts
* Customers
* Checkout
* Payments
* Orders

### Load API collection in Postman

Before you proceed to the exercises, get your API collection ready in the Postman:

* Download the free version of the Postman API tool.
* Download the Elastic Path Commerce Cloud Postman [Collection](postman/collection_20210720.json) and [Environment](postman/environment_20210720.json) files.
* Import the downloaded collection and environment into Postman.

>These instructions were developed for the `EPCC - July 20, 2021` collection version. This collection has been provided at the links above. You can find the most recently released Elastic Path Commerce Cloud Postman Collection [here](https://documentation.elasticpath.com/commerce-cloud/docs/developer/how-to/test-with-postman-collection.html). 

### Set up the Training Environment

You will be using the API key from your training store to create a new environment in Postman.
>You can request a training store by following the instructions from the [Request a Training Store](https://learn.elasticpath.com/learn/course/internal/view/elearning/11/epcc103-requesting-an-elastic-path-commerce-cloud-training-store) module.

* Go to https://dashboard.elasticpath.com/ and make note of your `Client ID` and `Client secret`.
* In Postman, complete the following steps:

    1. Select the `EPCC Environment` environment from the upper-right hand corner.
    2. Update environment variables named `clientID` and `clientSecret` using the values from the previous step.

As you progress through the collection, additional variables will be created and used from request to request. You can confirm the value of any variable's current value from the environment settings.

It is recommended you proceed through the instructions without taking side ventures. Otherwise the saved environment variables may not be the correct ones for subsequent steps. 

>Variables in Postman are in double curly braces `{{}}`.

### Exercises

If you are using the latest Elastic Path Commerce Cloud API with Product Content Management features, please complete the following exercises: 

* [1. Authentication](./authentication.md)
* [2. Products](./products.md)
* [3. Price Books](./pricebook.md)
* [4. Catalogs](./catalog.md)
* [5. Promotions](./promotions.md)
* [6. Cart](./cart.md)
* [7. Customers](./customers.md)
* [8. Checkout](./checkout.md)
* [9. Payment](./payment.md)
* [10. Orders](./order.md)

If you are using an older version of the Elastic Path Commerce Cloud API which uses the legacy `v2/products`, please complete these exercises: 

* [1. Authentication](legacy/authentication.md)
* [2. Products](legacy/products.md)
* [3. Settings](legacy/settings.md)
* [4. Promotions](legacy/promotions.md)
* [5. Cart](legacy/cart.md)
* [6. Customers](legacy/customers.md)
* [7. Checkout](legacy/checkout.md)
* [8. Payment](legacy/payment.md)
* [9. Orders](legacy/order.md)

