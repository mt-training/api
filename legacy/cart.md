## 5. Cart

In this exercise, you will add a product and a promotion to your cart. 

### Add product to cart

* Open the `Add product to cart` request under `carts -> cart_items` folder.
* Replace the `{{productID}}` with the id of the product with the sku `MTP-USED`. > The id you noted in the previous step.
* Update the quantity to 3.
* Click `Send`.
* Verify the response.

### Add promotion to cart

* Open the `Add a promotion` request under `carts -> cart_items` folder.
* Update the code to `SPECIAL`.
* Click `Send`.
* Verify the response.

[Next: 6. Customers](./customers.md)
