## 6. Customers

In this exercise, you will create a customer. 

### Create customer

* Open the `Create a customer` request under `customers` folder.
* Create a customer with the following fields: 
    * name: Rashid Wells
    * email: rwells@email.com
    * password: 1234qwer
* Click `Send`. The new customer id is saved to a `customerID` variable.
* Verify the response.

[Next: 7. Checkout](./checkout.md)
