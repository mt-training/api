## 7. Checkout

In this exercise, you will checkout. 

### Checkout

* Open the `Checkout as customer` request under `checkout` folder.
* Update the billing and shipping addresses for your new customer. 
* Click `Send`. The new order id is saved to a `orderID` variable.
* Verify the response.

[Next: 8. Payment](./payment.md)
