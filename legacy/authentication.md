## 1. Authentication

In this exercise, you will attempt to access an API endpoint without authorization, see the error, then authenticate properly to receive a client credentials token. 

### Attempt to get products list

* Open the `Get all products` request under `products_legacy` folder.
* Click `Send`. You should receive a 401 error like the following: 

```json
{
    "errors": [
        {
            "status": 401,
            "title": "Unable to validate access token"
        }
    ]
}
```

### Authenticate

* Open the `Get client credentials token` request under `authentication` folder.
* Click `Send`. There is a script that will save the authorization token received back in an `accessToken` variable in Postman.

> Client credentials token is valid for 30 minutes only. You can get a new token following the instructions above if you get a `401` error.

[Next: 2. Products](./products.md)
