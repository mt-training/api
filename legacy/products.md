## 2. Products

In this exercise, you will make a variety of CRUD calls to the Products endpoint. We will create a product, a brand, a variation, and generate child products. 

### Get products list

* Open the `Get all products` request under `products_legacy` folder.
* Click `Send` to get a full listing of products available in the training store.

### Create a new product

* Open the `Create a product` request under `products_legacy` folder.
* Create a product with the following fields: 
    * name: My Training Product
    * slug: my-training-product-{CONDITION}
    * sku: MTP-{CONDITION}
    * description: My Training Product
    * status: draft
    * price: 1299 in USD
* Leave the other fields at their defaults.
* Click `Send`. The new product id is saved to a `productID` variable.
* Verify the response.

> `{CONDITION}` is a placeholder that will be assigned a value later, using variations.

### Get a product 

* Open the `Get single product` request under `products_legacy` folder and click `Send`. 
* Verify the response matches the product you created above. 

### Create a new brand

* Open the `Create a brand` request under `brands_legacy` folder. 
* Create a brand with the following fields: 
    * name: My Training Brand
    * slug: my-training-brand
    * description: A brand for training purposes
    * status: live
* Click `Send`. The new brand id is saved to a `brandID` variable.
* Verify the response.

### Get a brand

* Open the `Get single brand` request under `brands_legacy` folder and click `Send`. 
* Verify the response matches the brand you created above.

### Create a product-brand relationship

* Create a new request with url `{{baseUrl}}/products/{{productID}}/relationships/brands`
* Copy the following into the body of the request:

```json
{
	"data": [
		{"type": "brand", "id": "{{brandID}}"}
	]
}
```
> Make sure you set `Authorization` type to Bearer Token with `{{accessToken}}` as the token and add `content-Type` header to `application/json`
* Click `Send`. 
* Verify the response.

### Get an invalid product

* Open the `Get single product` request under `products_legacy` folder.
* Replace the `{{productID}}` string in the call with `99999999-4554-4a4a-bbbb-123456789abc`.
* Click `Send`. 
* You should receive a 404 error like the following:

```json
{
    "errors": [
        {
            "status": 404,
            "detail": "The requested product could not be found",
            "title": "Product not found"
        }
    ]
}
```

### Create a variation

* Open the `Create a variation` request under `variations_legacy` folder.
* Copy the following into the body of the request:

```json
{
  "data": {
    "type": "product-variation",
    "name": "Condition"
  }
}
```

* Click `Send`.
> The script in the Tests section of the request, stores the returned ID in a variable called `variationID`.

### Create a product-variation relationship

* Create a new POST request with url `{{baseUrl}}/products/{{productID}}/relationships/variations`
* Copy the following into the body of the request:

```json
{
	"data": [
		{"type": "product-variation", "id": "{{variationID}}"}
	]
}
```

* Click `Send`. 
* Verify the response.

### Create options - New and Used

* Open the `Create a variation option` request under `variations_legacy` folder.
* Copy the following into the `Body` section of the request:

```json
{
  "data": {
    "type": "product-variation-option",
    "name": "New",
    "description": "New condition"
  }
}
```

* In the `Tests` section, replace `optionID` with `newOptionID`.
* Click `Send`.
> Returned option ID is saved in a `newOptionID` variable.

* Update the `Body` section to create your next option `Used`:
    * name: Used    
    * description: Used product
* Update the `Tests` section to save the option in `usedOptionID`.
* Click Send.

### Create modifiers - New and Used

* Open the `Create a product modifier` request under `variations_legacy` folder.
* Copy the following into the `Body` section of the request:

```json
{
  "data": {
    "type": "modifier",
    "modifier_type": "sku_builder",
    "value": {"seek":"CONDITION","set":"NEW"}
  }
}
```

* Click `Send`. 
* Verify the response.
* Repeat for modifier_type `slug_builder`.

Next, you need to create a modifier for `Used` option:

* Replace `newOptionID` in the url with `usedOptionID`.
* Update the `Body` section with:
    * modifier_type: sku_builder
    * value: {'seek':'CONDITION','set':'USED'}
* Click `Send`. 
* Verify the response.
* Repeat for modifier_type `slug_builder`.

### Build child products

* Open the `Build child products` request under `variations_legacy` folder.
* Copy the following into the `Body` section of the request:

```json
{
	"data": {
        "type": "product-variation", 
        "name": "Condition"
        }
}
```

* Click `Send`. 
* Verify the response.

### Update product status

* Open the `Update a product` request under `products_legacy` folder.
* Copy the following into the `Body` section:

```json
{
	"data": {
		"type": "product",
		"id": "{{productID}}",
		"status" : "live"
	}
}
```

* Click `Send`. 
* Verify the response matches the product you created above with the updated status.
* Now that the base product is live, you need to update the status of the child products. To do that, you can rebuild child products by repeating `Build child products` step above.

### Filter get products list

* Open the `Get all products` request under `products_legacy` folder.
* In the `Params` section, create `filter` and `include` parameters  with values `like(slug,my-training-product*)` and `brands` respectively.
* Click `Send`.
* In the response you will see the child products and the base product. You will also see the brand relationship as well as the complete brand information in the `included` element. 

[Next: 3. Settings](./settings.md)
