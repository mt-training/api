## 9. Orders

In this exercise, you will retrieve your order. 

### Get an order

* Open the `Get an order` request under `orders` folder.
* Click `Send`.
* Verify the response.