## 4. Catalog

In this exercise, you will configure then release/publish a catalog with the hierarchy and price book you created. 

### Configure a new catalog

* Open the `Create catalog` request under `catalogs -> catalog_configuration` folder.
* Create a catalog with the following fields: 
    * name: My Training Catalog
    * description: My Training Catalog
* The `hierarchyId` and `pricebookId` will autofill with your saved variables. You can add multiple hierarchies, but for this example you only have the one. 
* Click `Send`. The new catalog id is saved to a `catalogId` variable.
* Verify the response.

### Get a catalog 

* Open the `Get catalog by id` request under `catalogs -> catalog_configuration` folder and click `Send`. 
* Verify the response matches the catalog you created above. 

### Publish a catalog

* Open the `Create a release (publish)` request under `catalogs -> catalog_releases` folder and click `Send`.
*  The new catalog release id is saved to a `releaseId` variable.
* Verify the response.

### Get a released catalog 

* Open the `Get release by id` request under `catalogs -> catalog_releases` folder and click `Send`. 
* Verify the response matches the catalog release you created above. 

[Next: 5. Promotions](./promotions.md)
